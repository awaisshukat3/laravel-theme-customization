<header>
  <!-- header inner -->
  <div class="header">
      <div class="container">
          <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                  <div class="full">
                      <div class="center-desk">
                          <div class="logo"> <a href="{{url('/')}}"><img src="images/logo.jpg" alt="#"></a> </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <div class="menu-area">
                      <div class="limit-box">
                          <nav class="main-menu">
                              <ul class="menu-area-main">
                                  <li class="active"> <a href="{{url('/')}}">Home</a> </li>
                                  <li> <a href="{{url('/about-us')}}">About us</a> </li>
                                  <li> <a href="{{url('/meet-me')}}"> Meet me</a> </li>
                                  <li> <a href="{{url('/what-we-do')}}"> What we do</a> </li>
                                  <li> <a href="{{url('/contact-us')}}">Contact us</a> </li>
                                  <li> <a href="contact.html">Login </a> </li>
                                  <li class="mean-last"> <a href="#"><img src="images/search_icon.png" alt="#" /></a> </li>
                                  <li class="mean-last"> <a href="#"><img src="images/top-icon.png" alt="#" /></a> </li>
                              </ul>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- end header inner -->
</header>