<!DOCTYPE html>
<html lang="en">

<head>
 @include('partial.style')
</head>
<!-- body -->

<body class="main-layout">
    <!-- loader  -->
    <div class="loader_bg">
        <div class="loader"><img src="images/loading.gif" alt="#" /></div>
    </div>
    <!-- end loader -->
    <!-- header -->
   @include('partial.header')
    <!-- end header -->
    @yield('content')
    <!-- footer -->
    @include('partial.footer')
    <!-- end footer -->
    <!-- Javascript files-->
 @include('partial.js')
</body>

</html>