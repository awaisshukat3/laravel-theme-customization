<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('health.home');
});
Route::get('/about-us', function () {
    return view('health.about');
});
Route::get('/meet-me', function()  {
    return view('health.meet');
});

Route::get('/what-we-do', function() {
    return view('health.whatwedo');
});

Route::get('/contact-us', function(){
return view('health.contact');
});
